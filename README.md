## 📦 Installation

```bash
$ npm install
$ npm run pod

# or

$ yarn
$ yarn pod
```
