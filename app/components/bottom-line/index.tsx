import React from 'react';
import { View } from 'react-native';

import styles from './styles';

interface BottomLineProps {
  primary?: boolean;
}

const BottomLine: React.FC<BottomLineProps> = ({ primary }) => {
  return <View style={[styles.default, primary && styles.primary]} />;
};

BottomLine.defaultProps = {
  primary: false,
};

export default BottomLine;
