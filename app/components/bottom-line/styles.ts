import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  default: {
    alignSelf: 'center',
    height: 5,
    borderRadius: 2,
    width: 60,
    backgroundColor: 'rgba(255,255,255,0.1)',
    margin: 8,
  },
  primary: {
    width: '40%',
    marginTop: 24,
    backgroundColor: 'rgba(13, 71, 161,0.5)',
    height: 2,
  },
});
