import React from 'react';
import { View } from 'react-native';

import styles from './styles';

interface ButtonGroupRowProps {
  children?: React.ReactNode;
  width?: string | number;
}

const ButtonGroupRow: React.FC<ButtonGroupRowProps> = ({ children, width }) => {
  return <View style={[styles.row, { width }]}>{children}</View>;
};

ButtonGroupRow.defaultProps = {
  width: '35%',
};

export default ButtonGroupRow;
