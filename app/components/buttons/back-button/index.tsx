import React from 'react';
import { TouchableOpacity } from 'react-native';

import ArrowBack from '@components/icons/arraw-back';

import styles, { iconStyle } from './styles';

interface BackButtonProps {
  onPress: () => void;
}

const BackButton: React.FC<BackButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <ArrowBack width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

BackButton.defaultProps = {
  onPress: () => {},
};

export default BackButton;
