import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

import styles from './styles';

interface ChangeLanguageProps {
  onPress: () => void;
  children: React.ReactNode;
  minimal?: boolean;
}

const ChangeLanguage: React.FC<ChangeLanguageProps> = ({ onPress, children, minimal }) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress} style={[styles.button, minimal && styles.minimal]}>
      <Text style={styles.text}>{children}</Text>
    </TouchableOpacity>
  );
};

export default ChangeLanguage;
