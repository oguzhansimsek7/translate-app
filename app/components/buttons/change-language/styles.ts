import { StyleSheet } from 'react-native';

import colors from '@theme/colors';
import fonts from '@theme/fonts';

export default StyleSheet.create({
  button: {
    flex: 1,
    paddingVertical: 16,
    paddingHorizontal: 32,
    backgroundColor: 'rgba(255,255,255,0.03)',
    borderRadius: 10,
  },
  minimal: {
    paddingVertical: 10,
    borderRadius: 32,
  },
  text: {
    color: colors['light-blue'],
    fontSize: 16,
    fontFamily: fonts.Medium,
    textAlign: 'center',
  },
});
