import React from 'react';
import { TouchableOpacity } from 'react-native';

import Close from '@components/icons/close';

import styles, { iconStyle } from './styles';

interface CloseButtonProps {
  onPress: () => void;
}

const CloseButton: React.FC<CloseButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <Close width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

CloseButton.defaultProps = {
  onPress: () => {},
};

export default CloseButton;
