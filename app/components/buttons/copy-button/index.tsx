import React from 'react';
import { TouchableOpacity } from 'react-native';

import CopyContent from '@components/icons/copy-content';

import styles, { iconStyle } from './styles';

interface CopyButonProps {
  onPress: () => void;
}

const CopyButon: React.FC<CopyButonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <CopyContent width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

CopyButon.defaultProps = {
  onPress: () => {},
};

export default CopyButon;
