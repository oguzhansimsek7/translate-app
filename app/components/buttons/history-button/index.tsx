import React from 'react';
import { TouchableOpacity } from 'react-native';

import History from '@components/icons/history';

import styles, { iconStyle } from './styles';

interface HistoryButtonProps {
  onPress: () => void;
}

const HistoryButton: React.FC<HistoryButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <History width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

HistoryButton.defaultProps = {
  onPress: () => {},
};

export default HistoryButton;
