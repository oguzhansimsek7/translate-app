/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { TouchableOpacity } from 'react-native';

import MicNone from '@components/icons/mic-none';
import Mic from '@components/icons/mic';

import Voice from '@react-native-voice/voice';

import styles, { iconStyle } from './styles';

interface MicButtonProps {
  onChangeText: (value: string) => void;
  language?: string;
  onChangeStatus: (status: boolean) => void;
}

const MicButton: React.FC<MicButtonProps> = ({ onChangeText, language, onChangeStatus }) => {
  const [isRecord, setIsRecord] = React.useState<boolean>(false);

  const _onSpeechStart = () => {
    onChangeText('');
  };

  const _onSpeechEnd = () => {};
  const _onSpeechError = () => {};

  const _onSpeechResults = (event: any) => {
    onChangeText(event.value[0]);
  };

  const _onRecordVoice = () => {
    if (isRecord) {
      onChangeStatus(false);
      Voice.stop();
    } else {
      onChangeStatus(true);
      Voice.start(language);
    }
    setIsRecord(!isRecord);
  };

  React.useEffect(() => {
    Voice.onSpeechStart = _onSpeechStart;
    Voice.onSpeechEnd = _onSpeechEnd;
    Voice.onSpeechResults = _onSpeechResults;
    Voice.onSpeechError = _onSpeechError;

    return () => {
      Voice.destroy().then(Voice.removeAllListeners);
    };
  }, []);

  return (
    <TouchableOpacity activeOpacity={0.6} style={styles.button} onPress={_onRecordVoice}>
      {isRecord ? (
        <Mic height={42} width={42} fill={'rgb(183, 28, 28)'} />
      ) : (
        <MicNone height={iconStyle.height} width={iconStyle.width} fill={iconStyle.color} />
      )}
    </TouchableOpacity>
  );
};

MicButton.defaultProps = {
  language: 'tr-TR',
};

export default MicButton;
