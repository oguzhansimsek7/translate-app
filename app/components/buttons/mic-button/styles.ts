import { StyleSheet } from 'react-native';

import colors, { dark } from '@theme/colors';

export const iconStyle = {
  color: dark['button-text-color'],
  width: 32,
  height: 32,
};

export default StyleSheet.create({
  button: {
    alignSelf: 'center',
    width: 80,
    height: 80,
    borderRadius: 40,
    margin: 16,
    backgroundColor: 'rgba(48, 79, 254, 0.8)',
    borderColor: colors['light-blue'],
    justifyContent: 'center',
    alignItems: 'center',
  },
});
