import React from 'react';
import { TouchableOpacity } from 'react-native';

import MoreVerticalIcon from '@components/icons/more-vertical';

import styles, { iconStyle } from './styles';

interface MoreVerticalProps {
  onPress: () => void;
}

const MoreVertical: React.FC<MoreVerticalProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <MoreVerticalIcon width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

MoreVertical.defaultProps = {
  onPress: () => {},
};

export default MoreVertical;
