import React from 'react';
import { TouchableOpacity } from 'react-native';

import PasteContent from '@components/icons/paste-content';

import styles, { iconStyle } from './styles';

interface PasteButtonProps {
  onPress: () => void;
}

const PasteButton: React.FC<PasteButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <PasteContent width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

PasteButton.defaultProps = {
  onPress: () => {},
};

export default PasteButton;
