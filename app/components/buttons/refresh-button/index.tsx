import React from 'react';
import { TouchableOpacity } from 'react-native';

import Refresh from '@components/icons/refresh';

import styles, { iconStyle } from './styles';

interface RefreshButtonProps {
  onPress: () => void;
}

const RefreshButton: React.FC<RefreshButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <Refresh width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

RefreshButton.defaultProps = {
  onPress: () => {},
};

export default RefreshButton;
