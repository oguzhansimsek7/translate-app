import React from 'react';
import { TouchableOpacity } from 'react-native';

import Star from '@components/icons/star';
import StarBorder from '@components/icons/star-border';

import styles, { iconStyle } from './styles';

interface SavedButtonProps {
  fill?: boolean;
  onPress: () => void;
  color?: string;
  width?: number;
  height?: number;
}

const SavedButton: React.FC<SavedButtonProps> = ({ fill, color, onPress, width, height }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      {fill && (
        <Star width={width || iconStyle.width} height={height || iconStyle.height} fill={color || iconStyle.color} />
      )}
      {!fill && (
        <StarBorder width={width || iconStyle.width} height={height || iconStyle.height} fill={iconStyle.color} />
      )}
    </TouchableOpacity>
  );
};

SavedButton.defaultProps = {
  onPress: () => {},
};

export default SavedButton;
