import React from 'react';
import { TouchableOpacity } from 'react-native';

import Search from '@components/icons/search';

import styles, { iconStyle } from './styles';

interface SearchButtonProps {
  onPress: () => void;
}

const SearchButton: React.FC<SearchButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <Search width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

SearchButton.defaultProps = {
  onPress: () => {},
};

export default SearchButton;
