import React from 'react';
import { TouchableOpacity } from 'react-native';

import Settings from '@components/icons/settings';

import styles, { iconStyle } from './styles';

interface SettingsButtonProps {
  onPress: () => void;
}

const SettingsButton: React.FC<SettingsButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <Settings width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

SettingsButton.defaultProps = {
  onPress: () => {},
};

export default SettingsButton;
