import React from 'react';
import { TouchableOpacity } from 'react-native';

import SwapHoriz from '@components/icons/swap_horiz';

import styles, { iconStyle } from './styles';

interface SwapButtonProps {
  onPress: () => void;
}

const SwapButton: React.FC<SwapButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress} style={styles.button}>
      <SwapHoriz height={iconStyle.height} width={iconStyle.width} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

export default SwapButton;
