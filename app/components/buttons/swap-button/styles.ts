import { StyleSheet } from 'react-native';

export const iconStyle = {
  width: 24,
  height: 24,
  color: 'rgba(255,255,255,0.7)',
};

export default StyleSheet.create({
  button: {
    paddingHorizontal: 16,
  },
});
