import React from 'react';
import { TouchableOpacity } from 'react-native';

import VolumeUp from '@components/icons/volume-up';

import styles, { iconStyle } from './styles';

interface VoiceButtonProps {
  onPress: () => void;
}

const VoiceButton: React.FC<VoiceButtonProps> = ({ onPress }) => {
  return (
    <TouchableOpacity activeOpacity={0.8} onPress={onPress} style={styles.button}>
      <VolumeUp width={iconStyle.width} height={iconStyle.height} fill={iconStyle.color} />
    </TouchableOpacity>
  );
};

VoiceButton.defaultProps = {
  onPress: () => {},
};

export default VoiceButton;
