import * as React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

const Close = (props: SvgProps) => {
  return (
    <Svg height="24px" viewBox="0 0 24 24" width="24px" {...props}>
      <Path d="M0 0h24v24H0V0z" fill="none" />
      <Path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z" />
    </Svg>
  );
};

export default Close;
