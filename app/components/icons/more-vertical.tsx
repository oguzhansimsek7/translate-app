import * as React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

const Search = (props: SvgProps) => {
  return (
    <Svg height="24px" viewBox="0 0 24 24" width="24px" {...props}>
      <Path d="M0 0h24v24H0V0z" fill="none" />
      <Path d="M12 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z" />
    </Svg>
  );
};

export default Search;
