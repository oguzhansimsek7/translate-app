import * as React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

const NorthWest = (props: SvgProps) => {
  return (
    <Svg height="24px" viewBox="0 0 24 24" width="24px" {...props}>
      <Path fill="none" d="M0 0H24V24H0z" />
      <Path d="M5 15h2V8.41L18.59 20 20 18.59 8.41 7H15V5H5v10z" />
    </Svg>
  );
};

export default NorthWest;
