import * as React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

const Star = (props: SvgProps) => {
  return (
    <Svg height="24px" viewBox="0 0 24 24" width="24px" {...props}>
      <Path d="M12 17.27L18.18 21l-1.64-7.03L22 9.24l-7.19-.61L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27z" />
    </Svg>
  );
};

export default Star;
