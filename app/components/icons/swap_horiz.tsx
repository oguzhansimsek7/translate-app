import * as React from 'react';
import Svg, { Path, SvgProps } from 'react-native-svg';

function SwapHoriz(props: SvgProps) {
  return (
    <Svg height="24px" viewBox="0 0 24 24" width="24px" {...props}>
      <Path d="M0 0h24v24H0V0z" fill="none" />
      <Path d="M6.99 11L3 15l3.99 4v-3H14v-2H6.99v-3zM21 9l-3.99-4v3H10v2h7.01v3L21 9z" />
    </Svg>
  );
}

export default SwapHoriz;
