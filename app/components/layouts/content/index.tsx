import React from 'react';
import { SafeAreaView, View, useColorScheme } from 'react-native';

import styles from './styles';

interface ContentProps {
  children: React.ReactNode;
  transparent?: boolean;
  style?: object;
}

const Content: React.FC<ContentProps> = ({ children, transparent, style }) => {
  const isDark = useColorScheme() === 'dark';

  return (
    <View style={[styles.content, isDark ? styles.dark : styles.light, transparent && styles.transparent, style]}>
      <SafeAreaView style={!transparent && styles.safearea}>{children}</SafeAreaView>
    </View>
  );
};

export default Content;
