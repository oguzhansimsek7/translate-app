import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  content: {
    flex: 1,
    position: 'relative',
    backgroundColor: 'rgba(255,255,255,0.04)',
    borderBottomLeftRadius: 24,
    borderBottomRightRadius: 24,
    paddingHorizontal: 16,
  },
  safearea: { flex: 1 },
  dark: { backgroundColor: 'rgba(255,255,255,0.04)' },
  light: { backgroundColor: 'rgba(0,0,0,0.04)' },
  transparent: {
    flex: 0,
    backgroundColor: 'transparent',
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
});
