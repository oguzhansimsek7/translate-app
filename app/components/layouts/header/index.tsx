import React from 'react';
import { View } from 'react-native';

import styles from './styles';

interface HeaderProps {
  children: React.ReactNode;
}

const Header: React.FC<HeaderProps> = ({ children }) => {
  return <View style={styles.header}>{children}</View>;
};

export default Header;
