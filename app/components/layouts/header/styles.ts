import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  header: {
    paddingBottom: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
