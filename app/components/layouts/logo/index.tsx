import React from 'react';
import { Text } from 'react-native';

import styles from './styles';

const Logo = () => {
  return (
    <>
      <Text style={styles.text}>
        <Text style={styles.bold}>R</Text>ise
        <Text style={styles.bold}>T</Text>ranslate
      </Text>
    </>
  );
};

export default Logo;
