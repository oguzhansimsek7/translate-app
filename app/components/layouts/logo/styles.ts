import { StyleSheet } from 'react-native';

import fonts from '@theme/fonts';

export default StyleSheet.create({
  text: {
    fontSize: 20,
    color: 'rgba(255,255,255,0.7)',
    fontFamily: fonts.Light,
  },
  bold: {
    fontSize: 22,
    fontFamily: fonts.Medium,
  },
});
