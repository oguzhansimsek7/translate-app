import React from 'react';
import { TouchableWithoutFeedback, ActivityIndicator, View, useColorScheme, Keyboard } from 'react-native';

import styles from './styles';

interface PageProps {
  children: React.ReactNode;
  keyboarDismiss?: boolean;
  disabledForm?: boolean;
  loading?: boolean;
}

const Page: React.FC<PageProps> = ({ children, keyboarDismiss, disabledForm, loading }) => {
  const isDark = useColorScheme() === 'dark';

  if (loading) {
    return (
      <View style={[styles.container, styles.dark, styles.loading]}>
        <ActivityIndicator size="large" color={'rgba(255,255,255,0.8)'} />
      </View>
    );
  }

  if (disabledForm) {
    return <View style={[styles.container, isDark ? styles.dark : styles.light]}>{children}</View>;
  }

  return (
    <TouchableWithoutFeedback onPress={!keyboarDismiss ? Keyboard.dismiss : () => {}} accessible={false}>
      <View style={[styles.container, isDark ? styles.dark : styles.light]}>{children}</View>
    </TouchableWithoutFeedback>
  );
};

Page.defaultProps = {
  keyboarDismiss: false,
};

export default Page;
