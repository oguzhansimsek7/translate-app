import { StyleSheet } from 'react-native';

import { dark, light } from '@theme/colors';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  dark: { backgroundColor: dark['background-color'] },
  light: { backgroundColor: light['background-color'] },
  loading: { justifyContent: 'center', alignItems: 'center' },
});
