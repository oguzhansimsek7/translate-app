import React from 'react';
import { View, Text } from 'react-native';

import VoiceButton from '@components/buttons/voice-button';
import CopyButton from '@components/buttons/copy-button';
import styles from './styles';

interface ResultTitleProps {
  title: string;
}

const ResultTitle: React.FC<ResultTitleProps> = ({ title }) => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles['button-group']}>
        <CopyButton onPress={() => {}} />
        <VoiceButton onPress={() => {}} />
      </View>
    </View>
  );
};

export default ResultTitle;
