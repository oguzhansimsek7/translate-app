import fonts from '@theme/fonts';
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 24,
    paddingBottom: 24,
  },
  title: {
    fontSize: 18,
    fontFamily: fonts.Regular,
    color: 'rgba(255,255,255,0.6)',
  },
  'button-group': {
    flexDirection: 'row',
    alignItems: 'center',
    width: '20%',
    justifyContent: 'space-evenly',
  },
});
