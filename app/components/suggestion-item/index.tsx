import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';

import NorthWest from '@components/icons/north-west';

import styles, { iconStyles } from './styles';

interface SuggestionItemProps {
  onPress: () => void;
  value: string;
  translatedValue: string;
}

const SuggestionItem: React.FC<SuggestionItemProps> = ({ onPress, value, translatedValue }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles['suggestion-row']}>
      <View>
        <Text style={styles.value}>{value}</Text>
        <Text style={styles.translated}>{translatedValue}</Text>
      </View>
      <NorthWest width={iconStyles.width} height={iconStyles.height} fill={iconStyles.color} />
    </TouchableOpacity>
  );
};

export default SuggestionItem;
