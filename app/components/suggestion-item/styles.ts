import fonts from '@theme/fonts';
import { StyleSheet } from 'react-native';

export const iconStyles = {
  color: 'rgba(255,255,255,0.8)',
  width: 20,
  height: 20,
};

export default StyleSheet.create({
  'suggestion-row': {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 16,
  },
  value: {
    color: 'rgba(255,255,255,0.8)',
    lineHeight: 24,
    fontFamily: fonts.Regular,
  },
  translated: {
    color: 'rgb(79, 195, 247)',
    fontFamily: fonts.Regular,
  },
});
