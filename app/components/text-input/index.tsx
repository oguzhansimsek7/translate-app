import React from 'react';
import { View, TextInput, TextInputProps } from 'react-native';

import styles, { placeholderTextColor } from './styles';

const placeholderText = 'Metin girin';

export interface ITextInputProps {
  props: TextInputProps & { active: boolean };
  ref: React.Ref<TextInput>;
}

const CustomTextInput = React.forwardRef<ITextInputProps, any>(({ active, ...props }, ref) => {
  return (
    <View style={active ? styles.active : styles.wrapper}>
      <TextInput
        ref={ref}
        placeholder={placeholderText}
        placeholderTextColor={placeholderTextColor}
        style={styles.textarea}
        keyboardType="default"
        {...props}
      />
    </View>
  );
});

export default CustomTextInput;
