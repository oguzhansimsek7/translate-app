import { StyleSheet } from 'react-native';

import Fonts from '@theme/fonts';

export const placeholderTextColor = 'rgba(255,255,255,0.7)';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  textarea: {
    fontSize: 28,
    fontFamily: Fonts.Regular,
    color: 'rgba(255,255,255,0.8)',
  },
  active: {
    paddingBottom: 12,
  },
});
