import React from 'react';
import { Text, TextProps } from 'react-native';

import styles from './styles';

interface TranslatedTextProps extends TextProps {
  children: React.ReactNode;
}

const TranslatedText: React.FC<TranslatedTextProps> = ({ children }) => {
  return <Text style={styles.text}>{children}</Text>;
};

export default TranslatedText;
