import { StyleSheet } from 'react-native';

import Fonts from '@theme/fonts';

export default StyleSheet.create({
  text: {
    fontSize: 24,
    fontFamily: Fonts.Regular,
    color: 'rgb(144, 202, 249)',
    paddingVertical: 16,
  },
});
