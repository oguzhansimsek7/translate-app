import React from 'react';
import { View } from 'react-native';
import { useKeyboard } from '@react-native-community/hooks';

import { ChangeLanguage, SwapButton } from '@components';

import { LanguagesContext } from '../../provider/languages';

import styles from './styles';

const SwapLanguages: React.FC<{
  minimal: boolean;
  absolute?: boolean;
  navigation?: any;
}> = ({ minimal, absolute, navigation }) => {
  const keyboard = useKeyboard();

  const { language, swapLanguages } = React.useContext(LanguagesContext);

  return (
    <View style={[styles.row, absolute && { ...styles['keyboard-top'], bottom: keyboard.keyboardHeight }]}>
      <ChangeLanguage
        onPress={() => {
          navigation.navigate('ChangeLanguages', {
            key: 'source',
          });
        }}
        minimal={minimal}
      >
        {language?.source?.name}
      </ChangeLanguage>
      <SwapButton onPress={swapLanguages} />
      <ChangeLanguage
        onPress={() => {
          navigation.navigate('ChangeLanguages', {
            key: 'target',
          });
        }}
        minimal={minimal}
      >
        {language?.target?.name}
      </ChangeLanguage>
    </View>
  );
};

export default SwapLanguages;
