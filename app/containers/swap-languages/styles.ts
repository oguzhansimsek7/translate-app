import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  row: {
    paddingVertical: 16,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  'keyboard-top': {
    position: 'absolute',
  },
});
