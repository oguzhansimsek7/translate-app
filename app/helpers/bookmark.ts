import AsyncStorage from '@react-native-async-storage/async-storage';

const bookmarkStorageKey = '@bookmarks';

/* BOOKMARK ITEMS */
export const getItems = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const items = await AsyncStorage.getItem(bookmarkStorageKey);
      if (items) {
        resolve(JSON.parse(items));
      } else {
        resolve([]);
      }
    } catch (error) {
      reject(error);
    }
  });
};

export type ItemProps = {
  code: string;
  name: string;
  q: string;
};

export interface ItemDataProps {
  source: ItemProps;
  target: ItemProps;
}

/** SAVE BOOKMARK ITEM */
export const saveItem = (item: ItemDataProps) => {
  return new Promise(async (resolve, reject) => {
    try {
      const items: Array<ItemDataProps> | any = await getItems();
      items.push(item);
      await AsyncStorage.setItem(bookmarkStorageKey, JSON.stringify(items));
      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
};

/** DELETE BOOKMARK ITEM */
export const deleteItem = (params: ItemDataProps) => {
  return new Promise(async (resolve, reject) => {
    try {
      const items: any = await getItems();
      const index = items.findIndex((item: ItemDataProps) => item === params);
      items.splice(index, 1);
      await AsyncStorage.setItem(bookmarkStorageKey, JSON.stringify(items));
      resolve(true);
    } catch (error) {
      reject(error);
    }
  });
};

export const isBookmarked = (params: ItemDataProps) => {
  return new Promise(async (resolve, reject) => {
    try {
      const items: any = await getItems();
      const index = items.findIndex((item: ItemDataProps) => {
        return item === params;
      });
      resolve(index > -1);
    } catch (error) {
      reject(error);
    }
  });
};
