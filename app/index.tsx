import React from 'react';

import Navigations from './navigations';

import LanguagesProvider from './provider/languages';

const App = () => {
  return (
    <LanguagesProvider>
      <Navigations />
    </LanguagesProvider>
  );
};

export default App;
