import React from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { Home, Bookmarks, ChangeLanguages, Settings } from '@screens';

export type RootStackParams = {
  Home: undefined;
  Bookmarks: undefined;
  ChangeLanguages: { key: string };
  Settings: undefined;
};

const Stack = createNativeStackNavigator<RootStackParams>();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Bookmarks" component={Bookmarks} />
        <Stack.Screen
          name="ChangeLanguages"
          component={ChangeLanguages}
          initialParams={{
            key: 'source',
          }}
        />
        <Stack.Screen name="Settings" component={Settings} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
