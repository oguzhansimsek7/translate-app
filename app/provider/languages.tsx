import React from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';

import { getLanguages } from '@services/languages';

export type LanguageItem = {
  code: string;
  name: string;
};

export interface LanguageProps {
  source: LanguageItem;
  target: LanguageItem;
}

interface LanguagesContextProps {
  language: LanguageProps;
  languages: Array<LanguageItem>;
  onChangeLanguage: (key: string, value: LanguageItem) => void;
  swapLanguages: () => void;
  loading: boolean;
  retry: () => void;
  error: object;
}

const defaultValues: LanguageProps = {
  source: {
    code: 'tr',
    name: 'Turkish',
  },
  target: {
    code: 'en',
    name: 'English',
  },
};

const languageKey = '@language';
const languagesKey = '@languages';

export const LanguagesContext = React.createContext<LanguagesContextProps>({
  language: defaultValues,
  languages: [],
  onChangeLanguage: () => {},
  swapLanguages: () => {},
  loading: false,
  retry: () => {},
  error: {},
});

const LanguagesProvider: React.FC = ({ children }) => {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [error, setError] = React.useState<object>({});
  const [languages, setLanguages] = React.useState<Array<LanguageItem>>([]);

  const [language, setLanguage] = React.useState<LanguageProps>(defaultValues);

  const fetchLanguages = async () => {
    setLoading(true);
    getLanguages()
      .then((res: any) => {
        setLanguages(res);
        setLanguagesSetStorage(res);
        setLoading(false);
      })
      .catch((err) => {
        setError(err);
        setLoading(false);
      });
  };

  const setLanguagesSetStorage = async (value: Array<LanguageItem>) => {
    try {
      await AsyncStorage.setItem(languagesKey, JSON.stringify(value));
    } catch (err) {
      await AsyncStorage.setItem(languagesKey, JSON.stringify([]));
    }
  };

  const getLanguagesFromStorageOrServices = async () => {
    try {
      const data = await AsyncStorage.getItem(languagesKey);
      if (data) {
        setLanguages(JSON.parse(data));
      }
      if (!data) {
        await fetchLanguages();
      }
    } catch (err) {
      await fetchLanguages();
    }
  };

  /** SET LANGUAGES TO STORAGE */
  const setLanguageToStorage = async (value: LanguageProps) => {
    try {
      await AsyncStorage.setItem(languageKey, JSON.stringify(value));
    } catch (err) {
      await AsyncStorage.setItem(languageKey, JSON.stringify(defaultValues));
    }
  };

  /** GET DEFAULT LANGUAGES FROM STORAGE */
  const getLanguageFromStorage = async () => {
    try {
      const storageLanguages = await AsyncStorage.getItem(languageKey);
      if (storageLanguages) {
        setLanguage(JSON.parse(storageLanguages));
      } else {
        setLanguage(defaultValues);
        setLanguageToStorage(defaultValues);
      }
    } catch (err) {
      setLanguage(defaultValues);
      setLanguageToStorage(defaultValues);
    }
  };

  React.useEffect(() => {
    getLanguagesFromStorageOrServices();
    getLanguageFromStorage();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  /** LANGUAGES ONCHANGE */
  const onChangeLanguage = (key: string, value: LanguageItem): void => {
    if (!key) {
      throw new Error('Key is not defined');
    }
    const values: LanguageProps = {
      ...language,
      [key]: value,
    };
    setLanguage(values);
    setLanguageToStorage(values);
  };

  /** LANGUAGES SWAP */
  const swapLanguages = () => {
    const values: LanguageProps = {
      source: language.target,
      target: language.source,
    };
    setLanguage(values);
    setLanguageToStorage(values);
  };

  return (
    <LanguagesContext.Provider
      value={{
        language,
        languages,
        onChangeLanguage,
        swapLanguages,
        loading,
        retry: fetchLanguages,
        error,
      }}
    >
      {children}
    </LanguagesContext.Provider>
  );
};

export default LanguagesProvider;
