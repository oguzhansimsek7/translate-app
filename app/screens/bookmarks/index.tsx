import React from 'react';
import { FlatList, Text, View } from 'react-native';

import {
  BackButton,
  Content,
  Header,
  Page,
  SearchButton,
  RefreshButton,
  ButtonGroupRow,
  SavedButton,
} from '@components';

import { getItems, deleteItem, ItemDataProps } from '@helpers/bookmark';

import styles, { buttonColor } from './styles';

const Bookmarks: React.FC<{ navigation?: any }> = ({ navigation }) => {
  const [savedList, setSavedList] = React.useState<any>([]);

  React.useEffect(() => {
    getItems()
      .then((res) => {
        setSavedList(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const removeItem = (index: number, item: ItemDataProps) => {
    const newSavedList = [...savedList];
    newSavedList.splice(index, 1);
    setSavedList(newSavedList);
    deleteItem(item);
  };

  return (
    <Page disabledForm={true}>
      <Content transparent>
        <Header>
          <BackButton
            onPress={() => {
              navigation.goBack();
            }}
          />
          <ButtonGroupRow width="22%">
            <SearchButton onPress={() => {}} />
            <RefreshButton onPress={() => {}} />
          </ButtonGroupRow>
        </Header>
        <View style={styles['list-wrapper']}>
          <Text style={styles['page-title']}>Kaydedilenler</Text>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={savedList}
            renderItem={({ item, index }) => {
              return (
                <View key={index.toString()} style={styles['row-item']}>
                  <View>
                    <Text style={styles.target}>{item.source.q}</Text>
                    <Text style={styles.value}>{item.target.q}</Text>
                  </View>
                  <SavedButton
                    fill
                    width={26}
                    height={26}
                    color={buttonColor}
                    onPress={() => {
                      removeItem(index, item);
                    }}
                  />
                </View>
              );
            }}
          />
        </View>
      </Content>
    </Page>
  );
};

export default Bookmarks;
