import { StyleSheet } from 'react-native';

import fonts from '@theme/fonts';

export const buttonColor = 'rgba(33, 150, 243, 0.7)';

export default StyleSheet.create({
  'page-title': {
    fontSize: 28,
    fontFamily: fonts.Regular,
    color: 'rgba(255,255,255,0.7)',
    marginVertical: 32,
  },
  'list-wrapper': { height: '95%' },
  'row-item': {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  target: {
    fontSize: 17,
    fontFamily: fonts.Regular,
    color: 'rgba(255,255,255,0.7)',
  },
  value: {
    fontSize: 15,
    fontFamily: fonts.Light,
    color: 'rgba(255,255,255,0.6)',
  },
});
