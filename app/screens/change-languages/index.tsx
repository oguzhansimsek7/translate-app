import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { BackButton, Content, Header, Page, SearchButton, RefreshButton, ButtonGroupRow } from '@components';

import { Done } from '@components/icons';

import { LanguagesContext, LanguageProps } from '../../provider/languages';

import styles from './styles';

const ChangeLanguage: React.FC<{ route: any }> = ({ route }) => {
  const navigation = useNavigation();
  const { key } = route.params;

  const { language, loading, retry, languages, onChangeLanguage } = React.useContext(LanguagesContext);

  return (
    <Page loading={loading}>
      <Content transparent>
        <Header>
          <BackButton
            onPress={() => {
              navigation.goBack();
            }}
          />
          <ButtonGroupRow width="22%">
            <SearchButton onPress={() => {}} />
            <RefreshButton
              onPress={() => {
                retry();
              }}
            />
          </ButtonGroupRow>
        </Header>
        <Text style={styles['page-title']}>{key === 'source' ? 'Şu dilden çevir:' : 'Şu dile çevir:'}</Text>
        <Text style={styles['language-title']}>TÜM DİLLER</Text>
        <View style={styles['list-wrapper']}>
          <FlatList
            data={languages}
            keyExtractor={(item) => item.code}
            renderItem={({ item, index }) => {
              const isSelectedLanguage = item.code === language[key as keyof LanguageProps].code;
              return (
                <TouchableOpacity
                  key={index.toString()}
                  onPress={() => {
                    onChangeLanguage(key, item);
                    navigation.goBack();
                  }}
                  style={[styles['language-row'], isSelectedLanguage && styles['active-langulage-row']]}
                >
                  {isSelectedLanguage && <Done fill="rgb(13, 71, 161)" />}
                  <Text style={[styles['language-row-text'], isSelectedLanguage && styles['active-language-row-text']]}>
                    {item.name}
                  </Text>
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </Content>
    </Page>
  );
};

export default ChangeLanguage;
