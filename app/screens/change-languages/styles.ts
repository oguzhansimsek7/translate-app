import { StyleSheet } from 'react-native';

import fonts from '@theme/fonts';

export default StyleSheet.create({
  'page-title': {
    fontSize: 28,
    fontFamily: fonts.Regular,
    color: 'rgba(255,255,255,0.9)',
    marginTop: 32,
  },
  'language-title': {
    fontSize: 14,
    fontFamily: fonts.Medium,
    color: 'rgb(21, 101, 192)',
    marginTop: 32,
    marginBottom: 16,
  },
  'list-wrapper': { height: '78%' },
  'language-row': {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'center',
    paddingVertical: 14,
    paddingHorizontal: 8,
    borderRadius: 32,
  },
  'active-langulage-row': {
    paddingHorizontal: 24,
    paddingVertical: 12,
    backgroundColor: 'rgb(130, 177, 255)',
  },
  'language-row-text': {
    fontSize: 18,
    color: 'rgba(255,255,255,0.8)',
    fontFamily: fonts.Regular,
    marginLeft: 0,
  },
  'active-language-row-text': {
    color: 'rgb(13, 71, 161)',
    marginLeft: 12,
  },
});
