/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';

import { Keyboard } from 'react-native';

import {
  Page,
  Content,
  Header,
  MicButton,
  SavedButton,
  TextInput,
  TranslatedText,
  SettingsButton,
  BackButton,
  CloseButton,
  BottomLine,
  ResultTitle,
  HistoryButton,
  MoreVerticalButton,
  ButtonGroupRow,
} from '@components';

import { SwapLanguages } from '@containers';
import { translate } from '@services/languages';

import { LanguagesContext } from '../../provider/languages';

import { saveItem, isBookmarked } from '@helpers/bookmark';

const Home: React.FC<{ navigation?: any }> = ({ navigation }) => {
  const { language } = React.useContext(LanguagesContext);

  const inputRef = React.useRef<any>(null);

  const [isFocused, setFocused] = React.useState<boolean>(false);
  const [resultSummary, setResultSummary] = React.useState<boolean>(false);

  const [value, setValue] = React.useState<string>('');
  const [result, setResult] = React.useState<string>('');
  const [placeholder, setPlaceholder] = React.useState('Metin girin');

  const [bookMarked, setBookMarked] = React.useState(false);

  React.useEffect(() => {
    if (value) {
      translate(value, language.source.code, language.target.code)
        .then((res: any) => {
          setResult(res);
        })
        .catch(() => {
          setResult('');
        });
    }
  }, [value]);

  React.useEffect(() => {
    setValue(result);
  }, [language.source]);

  const _goBack = () => {
    inputRef?.current?.blur();
    setResultSummary(false);
    setFocused(false);
    setValue('');
    setResult('');
  };

  const _onCleanText = () => {
    setValue('');
    setResult('');
  };

  React.useEffect(() => {
    if (resultSummary) {
      isBookmarked({
        source: {
          code: language.source.code,
          name: language.source.name,
          q: value,
        },
        target: {
          code: language.target.code,
          name: language.target.name,
          q: result,
        },
      })
        .then((res: any) => {
          setBookMarked(res);
        })
        .catch(() => {
          setBookMarked(false);
        });
    }
  }, [resultSummary, result]);

  return (
    <Page keyboarDismiss={!!value}>
      <Content>
        {!isFocused && !resultSummary && (
          <Header>
            <SavedButton
              fill={true}
              onPress={() => {
                navigation.navigate('Bookmarks');
              }}
            />
            <ButtonGroupRow width="22%">
              <HistoryButton onPress={() => {}} />
              <SettingsButton
                onPress={() => {
                  navigation.navigate('Settings');
                }}
              />
            </ButtonGroupRow>
          </Header>
        )}
        {isFocused && !resultSummary && (
          <Header>
            <BackButton onPress={_goBack} />
            <ButtonGroupRow>
              <HistoryButton onPress={() => {}} />
              <CloseButton onPress={_onCleanText} />
              <MoreVerticalButton onPress={() => {}} />
            </ButtonGroupRow>
          </Header>
        )}
        {resultSummary && (
          <Header>
            <BackButton onPress={_goBack} />
            <ButtonGroupRow>
              <HistoryButton onPress={() => {}} />
              <SavedButton
                fill={bookMarked}
                color={bookMarked ? 'rgba(33, 150, 243, 0.7)' : 'transparent'}
                onPress={() => {
                  setBookMarked(true);
                  saveItem({
                    source: {
                      code: language.source.code,
                      name: language.source.name,
                      q: value,
                    },
                    target: {
                      code: language.target.code,
                      name: language.target.name,
                      q: result,
                    },
                  });
                }}
              />
              <MoreVerticalButton onPress={() => {}} />
            </ButtonGroupRow>
          </Header>
        )}
        {resultSummary && <ResultTitle title={language.source.name} />}
        <TextInput
          ref={inputRef}
          value={value}
          active={!!value}
          placeholder={placeholder}
          selectTextOnFocus={false}
          onChangeText={(text: string) => {
            setValue(text);
          }}
          onFocus={() => {
            setFocused(true);
            setResultSummary(false);
          }}
          onBlur={() => {
            setFocused(false);
          }}
          onSubmitEditing={() => {
            Keyboard.dismiss();
            setFocused(false);
            setResultSummary(true);
          }}
        />

        <BottomLine primary={!!value} />
        {resultSummary && <ResultTitle title={language.target.name} />}
        {!!result && <TranslatedText>{result}</TranslatedText>}
      </Content>
      <Content transparent>
        <SwapLanguages minimal={!!isFocused} absolute={isFocused} navigation={navigation} />
        <MicButton
          language={language.source.code}
          onChangeStatus={(status) => {
            if (status) {
              setValue('');
              setResult('');
              setResultSummary(false);
              setPlaceholder('Birşeyler söyleyin...');
            } else {
              setPlaceholder('Metin girin');
            }
          }}
          onChangeText={(text) => {
            if (text) {
              setValue(text);
              setFocused(false);
              setResultSummary(true);
            }
          }}
        />
      </Content>
    </Page>
  );
};

export default Home;
