import { StyleSheet } from 'react-native';

import { dark, light } from '@theme/colors';

export default StyleSheet.create({
  container: { flex: 1 },
  'text-dark': {
    color: dark['text-color'],
  },
  'text-light': {
    color: light['text-color'],
  },
});
