export { default as Home } from './home';
export { default as ChangeLanguages } from './change-languages';
export { default as Bookmarks } from './bookmarks';
export { default as Settings } from './settings';
