import React from 'react';
import { Text } from 'react-native';

import { BackButton, Content, Header, MoreVerticalButton, Page } from '@components';

import styles from './styles';

const Settings: React.FC<{ navigation?: any }> = ({ navigation }) => {
  return (
    <Page disabledForm={true}>
      <Content transparent>
        <Header>
          <BackButton
            onPress={() => {
              navigation.goBack();
            }}
          />
          <MoreVerticalButton onPress={() => {}} />
        </Header>
        <Text style={styles['page-title']}>Settings</Text>
      </Content>
    </Page>
  );
};

export default Settings;
