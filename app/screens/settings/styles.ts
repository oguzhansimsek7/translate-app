import { StyleSheet } from 'react-native';

import fonts from '@theme/fonts';

export default StyleSheet.create({
  'page-title': {
    fontSize: 28,
    fontFamily: fonts.Regular,
    color: 'rgba(255,255,255,0.7)',
    marginVertical: 32,
  },
});
