import axios from 'axios';

// BaseUrl
const baseURL = 'https://libretranslate.de';

// Axios Instance
const instance = axios.create({
  baseURL,
  headers: { 'Content-Type': 'application/json' },
});

/* GET SUPPORTED LANGUAGES */
export const getLanguages = () => {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await instance.get('/languages');
      resolve(response.data);
    } catch (error) {
      reject(error);
    }
  });
};

/* DETECT LANGUAGE */
export const detectLanguage = (params: string, defaultLang: string) => {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await instance.post('/detect', { q: params });
      if (defaultLang !== response.data[0].language) {
        resolve(false);
      } else {
        resolve(true);
      }
    } catch (error) {
      reject(error);
    }
  });
};

/* TRANSLATE */
export const translate = (params: string, source: string, target: string) => {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await instance.post('/translate', { q: params, source, target, format: 'text' });
      resolve(response.data.translatedText);
    } catch (error) {
      reject(error);
    }
  });
};
