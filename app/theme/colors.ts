const colors = {
  black: '#17181f',
  gray: '#535356',
  blue: '#0B27FD',
  'light-blue': '#A8C2CD',
  light: '#877582',
  white: '#EDEEEF',
};

export default colors;

export const dark = {
  'background-color': colors.black,
  'button-color': colors.blue,
  'button-text-color': colors['light-blue'],
  'text-color': colors.white,
  'icon-color': colors.white,
};

export const light = {
  'background-color': colors.white,
  'button-color': colors.blue,
  'button-text-color': colors['light-blue'],
  'text-color': colors.black,
  'icon-color': colors.gray,
};
