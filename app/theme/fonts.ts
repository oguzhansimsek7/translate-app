export default {
  Light: 'Roboto-Light',
  Regular: 'Roboto-Regular',
  Medium: 'Roboto-Medium',
};
