module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./app'],
        extensions: ['.ios.ts', '.android.ts', '.ts', '.tsx', '.ios.tsx', '.android.tsx', '.jsx', '.js', '.json'],
        alias: {
          '@assets': './app/assets/',
          '@components': './app/components/',
          '@containers': './app/containers/',
          '@helpers': './app/helpers/',
          '@navigations': './app/navigations/',
          '@screens': './app/screens/',
          '@theme': './app/theme/',
          '@provider': './app/provider/',
          '@services': './app/services/',
        },
      },
    ],
  ],
};
